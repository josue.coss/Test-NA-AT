package com.example.testnat.utilities

class Constants {
    companion object {
        const val PREFS_NAME = "Preferences"
        const val GRANT_TYPE = "grant_type"
        const val USER_NAME = "username"
        const val PASSWORD = "password"
        const val USERNAME_PREFERENCE_KEY = "username"
        const val PASSWORD_PREFERENCE_KEY = "password"
        const val TOKEN_PREFERENCE_KEY = "token"
        const val TOKEN_ACCESS_PREFERENCE_KEY = "tokenAccess"
        const val TOKEN_REFRESH_PREFERENCE_KEY = "tokenRefresh"
        const val EXPIRES_IN_PREFERENCE_KEY = "expiresIn"
        const val TOKEN_TYPE_PREFERENCE_KEY = "tokenType"
        const val SCOPE_PREFERENCE_KEY = "scope"
        const val JTI_PREFERENCE_KEY = "jti"
        const val BASIC_AUTH =
            "Wm1Ga0xXTXlZeTF3YjNKMFlXdz06TWpoa04yUTNNbUppWVRWbVpHTTBObVl4Wmpka1lXSmpZbVEyTmpBMVpEVXpaVFZoT1dNMVpHVTROakF4TldVeE9EWmtaV0ZpTnpNd1lUUm1ZelV5WWc9PQ=='\\"
        const val RECARGA_COMPONENT_TAG = "recargaComponentTag"
        const val EXITO_COMPONENT_TAG = "exitoComponentTag"
        const val DAYTIME_ONLY_DATE = "dd-MM-yyyy"
        const val HOURS_FORMAT = "HH:mm"
        const val LANGUAGE_TAG_ES = "es-ES"
        const val DATABASE_NAME = "nat-db"
        const val DATABASE_MARCAS = "login"
    }
}