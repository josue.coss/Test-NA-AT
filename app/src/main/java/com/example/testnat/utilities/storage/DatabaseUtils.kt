package com.example.testnat.utilities.storage

import android.util.Log
import com.example.testnat.models.InfoMarcas
import com.example.testnat.models.Marcas
import com.example.testnat.utilities.Constants.Companion.DATABASE_MARCAS
import kotlinx.coroutines.*

class DatabaseUtils {
    interface MarcasDBListener {
        suspend fun startInsertContent(
            marcaContent: Marcas?,
            appDatabase: AppDatabase?
        ) {
            coroutineScope {
                launch {
                    val marca = marcaContent?.marca?.let {
                        InfoMarcas(
                            it,
                            marcaContent.subMarca1,
                            marcaContent.subMarca3,
                            marcaContent.subMarca3
                        )
                    }
                    if (marca != null) {
                        insertContentMarcas(marca, appDatabase, this@MarcasDBListener)
                    }
                }
            }
            /*GlobalScope.launch(Dispatchers.Main) {
                withContext(Dispatchers.IO) {
                    val marcasContent = InfoMarcas(
                        marcasContent?.marca,
                        marcasContent?.subMarca1,
                        marcasContent?.subMarca3,
                        marcasContent?.subMarca3
                    )
                    insertContentMarcas(marcasContent, appDatabase, this@MarcasDBListener)
                }
            }*/
        }

        suspend fun startGetMarcasContent(database: AppDatabase?) {
            getContentMarcas(database, this@MarcasDBListener)
        }

        fun onFinishedinsertContent(flag: Boolean)
        fun onFinishedGetContent(content: Any?, finished: String?)
    }

    companion object {
        var marcasContent: List<InfoMarcas> = listOf()
        //var marcasContent: InfoMarcas? = null

        suspend fun getContentMarcas(
            database: AppDatabase?,
            marcasDBListener: MarcasDBListener
        ) {
            coroutineScope {
                /*launch {
                    marcasContent = withContext(Dispatchers.IO) {
                        database!!.contentMarcasDao().getInfoMarcas()
                    }
                }.invokeOnCompletion {
                    database?.close()
                    if (marcasContent.isNotEmpty()) {
                        marcasDBListener.onFinishedGetContent(marcasContent, DATABASE_MARCAS)
                        Log.d("GET FINISHED", "OK")
                    }
                }*/
                GlobalScope.launch(Dispatchers.Main) {
                    marcasContent = withContext(Dispatchers.IO) {
                        database!!.contentMarcasDao().getInfoMarcas()
                    }
                }.invokeOnCompletion {
                    database?.close()
                    if (marcasContent.isNotEmpty()) {
                        marcasDBListener.onFinishedGetContent(marcasContent, DATABASE_MARCAS)
                        Log.d("GET FINISHED", "OK")
                    }
                }
            }

        }

        suspend fun insertContentMarcas(
            marcaContent: InfoMarcas,
            database: AppDatabase?,
            listener: MarcasDBListener
        ) {
            /*coroutineScope {
                launch {
                    database?.contentMarcasDao()?.insertMarca(marcaContent)
                }.invokeOnCompletion {
                    listener.onFinishedinsertContent(true)
                    Log.d("INSERTS FINISHED", "OK")
                }
            }*/
            GlobalScope.launch(Dispatchers.Main) {
                withContext(Dispatchers.IO) {
                    database!!.contentMarcasDao().insertMarca(marcaContent)
                }
            }.invokeOnCompletion {
                listener.onFinishedinsertContent(true)
                Log.d("INSERTS FINISHED", "OK")
            }
        }
    }
}