package com.example.testnat.utilities

import android.util.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DateUtils {
    companion object {

        val TAG = javaClass.name

        fun getTodayDateWithFormat(format: String, locale: Locale): String {
            val formatter = SimpleDateFormat(format, locale)
            return formatter.format(Date())
        }

        fun getDateWithFormat(format: String, date: Date?, locale: Locale): String {
            return try {
                val formatter = SimpleDateFormat(format, locale)
                formatter.format(date)
            } catch (e: ParseException) {
                Log.e(TAG, e.toString())
                ""
            } catch (e: NullPointerException) {
                Log.e(TAG, e.toString())
                ""
            }
        }
    }
}