package com.example.testnat.utilities.storage

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.testnat.models.InfoMarcas
import com.example.testnat.models.daos.InfoMarcasDao
import com.example.testnat.utilities.Constants.Companion.DATABASE_NAME

@Database(
    entities = [InfoMarcas::class], version = 1
)

abstract class AppDatabase : RoomDatabase() {
    abstract fun contentMarcasDao(): InfoMarcasDao

    companion object {
        private var instance: AppDatabase? = null


        fun getInstance(context: Context): AppDatabase? {
            return instance
                ?: synchronized(this) {
                    instance
                        ?: Room.databaseBuilder(
                            context.applicationContext,
                            AppDatabase::class.java, DATABASE_NAME
                        ).build()
                }
        }

        fun destroyInstance() {
            instance = null
        }
    }
}