package com.example.testnat.utilities

import android.content.Context
import android.content.SharedPreferences
import com.example.testnat.modules.base.BaseApplication
import com.example.testnat.utilities.Constants.Companion.PREFS_NAME

class SharedPreferencesUtils {
        companion object {

            val sharedPref =
                BaseApplication.getContext()?.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

            fun setDataString(KEY_NAME: String, value: String) {
                val editor: SharedPreferences.Editor? = sharedPref?.edit()
                editor?.putString(KEY_NAME, value)
                editor?.apply()
            }

            fun getDataString(KEY_NAME: String): String? {
                return sharedPref?.getString(KEY_NAME, "")
            }

            fun clearSharedPreference() {
                val editor: SharedPreferences.Editor? = sharedPref?.edit()
                editor?.clear()
                editor?.apply()
            }
        }
}