package com.example.testnat.modules.login

import com.example.testnat.models.ResponseLogin

interface LoginContract {
    interface Model {
        interface OnFinishedListener {
            fun onFinishedLogin(response: ResponseLogin?)
            fun onFailure(t: Throwable?)
            fun onFailureMessage(message: String?)
        }

        fun login(
            username: String?,
            password: String?,
            onFinishedListener: OnFinishedListener
        )
    }

    interface Presenter {
        fun login(
            username: String?,
            password: String?
        )
    }

    interface View {
        fun initViews()
        fun onFinishedLogin(response: ResponseLogin?)
        fun onResponseFailure(throwable: Throwable?)
        fun onResponseMessage(message: String?)
    }
}