package com.example.testnat.modules.login

import com.example.testnat.models.ResponseLogin

class LoginPresenter constructor(view: LoginContract.View?) : LoginContract.Presenter,
    LoginContract.Model.OnFinishedListener {
    val model: LoginModel = LoginModel()
    val view: LoginContract.View? = view

    override fun login(username: String?, password: String?) {
        model.login(username, password, this)
    }

    override fun onFinishedLogin(response: ResponseLogin?) {
        view?.onFinishedLogin(response)
    }

    override fun onFailure(t: Throwable?) {
        view?.onResponseFailure(t)
    }

    override fun onFailureMessage(message: String?) {
        view?.onResponseMessage(message)
    }
}