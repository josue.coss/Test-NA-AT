package com.example.testnat.modules.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.testnat.R
import com.example.testnat.components.ExitoComponent
import com.example.testnat.components.RecargaComponent
import com.example.testnat.listeners.OnOkListener
import com.example.testnat.utilities.Constants.Companion.DAYTIME_ONLY_DATE
import com.example.testnat.utilities.Constants.Companion.EXITO_COMPONENT_TAG
import com.example.testnat.utilities.Constants.Companion.HOURS_FORMAT
import com.example.testnat.utilities.Constants.Companion.LANGUAGE_TAG_ES
import com.example.testnat.utilities.Constants.Companion.RECARGA_COMPONENT_TAG
import com.example.testnat.utilities.DateUtils.Companion.getDateWithFormat
import com.example.testnat.utilities.DateUtils.Companion.getTodayDateWithFormat
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.fragment_recargas.*
import java.util.*

class RecargasFragment : Fragment(R.layout.fragment_recargas) {

    private var user: String? = ""
    private var hour: String = ""
    private var date: String = ""
    private var phone: String? = null
    private var cash: String? = null
    private var dialogoRecargaComponent: RecargaComponent? = null
    private var dialogoExitoComponent: ExitoComponent? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_recargas, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initViews() {
        val currentTime: Date? = Calendar.getInstance().time
        val locale = Locale(LANGUAGE_TAG_ES)
        user = getString(R.string.user2)
        hour = getDateWithFormat(
            HOURS_FORMAT,
            currentTime,
            locale
        )
        date = getTodayDateWithFormat(DAYTIME_ONLY_DATE, Locale("es", "ES"))
        button_next.setOnClickListener {
            if (validateForm()) {
                showRecargaComponent()
            }
        }
        layoutBack.setOnClickListener {
            nextFragment()
        }
        button_back.setOnClickListener {
            nextFragment()
        }
    }

    private fun validateForm(): Boolean {
        if (!validateInputSize(inputPhone, getString(R.string.error_phone_null), 0)) {
            return false
        }
        if (!validateInputSize(inputCash, getString(R.string.error_cash_null), 0)) {
            return false
        }
        phone = inputPhone.text.toString().trim()
        cash = inputCash.text.toString().trim()
        return true
    }

    private fun validateInputSize(
        editText: TextInputEditText,
        messageError: String?,
        size: Int,
    ): Boolean {
        return when {
            (editText.text == null) || (editText.length() <= size) -> {
                editText.error = messageError
                false
            }
            else -> {
                editText.error = null
                true
            }
        }
    }

    private fun showRecargaComponent() {
        dialogoRecargaComponent = activity?.let {
            phone?.let { it1 ->
                cash?.let { it2 ->
                    user?.let { it3 ->
                        RecargaComponent(
                            it,
                            object : OnOkListener {
                                override fun onOk(tag: String) {
                                    if (tag == RECARGA_COMPONENT_TAG)
                                        showExitoComponent()
                                }
                            },
                            it3,
                            hour,
                            date,
                            it1,
                            it2
                        )
                    }
                }
            }
        }
        dialogoRecargaComponent?.show()
    }

    private fun showExitoComponent() {
        dialogoExitoComponent = activity?.let {
            ExitoComponent(
                it,
                object : OnOkListener {
                    override fun onOk(tag: String) {
                        if (tag == EXITO_COMPONENT_TAG)
                            nextFragment()
                    }
                }
            )
        }
        dialogoExitoComponent?.show()
    }

    private fun nextFragment() {
        view?.findNavController()?.navigate(R.id.action_recargasFragment_to_marcasFragment)
    }
}