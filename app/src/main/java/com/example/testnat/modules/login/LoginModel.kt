package com.example.testnat.modules.login

import android.content.res.Resources
import android.util.Log
import com.example.testnat.R
import com.example.testnat.models.Marcas
import com.example.testnat.models.ResponseLogin
import com.example.testnat.modules.base.BaseApplication
import com.example.testnat.services.RetrofitClient
import com.example.testnat.utilities.storage.DatabaseUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.CoroutineContext

class LoginModel : LoginContract.Model, CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job
    private lateinit var job: Job

    override fun login(
        username: String?,
        password: String?,
        onFinishedListener: LoginContract.Model.OnFinishedListener
    ) {
        job = Job()

        val apiService: Call<ResponseLogin> =
            RetrofitClient.getInstance(RetrofitClient.ClientType.BASE_WITH_AUTH).api.login(
                username = username,
                password = password
            )

        val dbUtils = object : DatabaseUtils.MarcasDBListener {
            override fun onFinishedinsertContent(flag: Boolean) {
                Log.d("onFinishedinsertContent", flag.toString())
            }

            override fun onFinishedGetContent(content: Any?, finished: String?) {

            }

        }

        apiService.enqueue(object : Callback<ResponseLogin> {
            override fun onResponse(
                call: Call<ResponseLogin>,
                response: Response<ResponseLogin>
            ) {
                if (response.isSuccessful) {
                    if (response.code() == 200) {
                        launch {
                            val listMarcas: MutableList<Marcas> = mutableListOf()
                            val marcaClaro = "CLARO"
                            val marcaTuenti = "TUENTI"
                            val marcaEntel = "ENTEL"
                            val tiempoAire = "Tiempo aire"
                            val megas = "Megas"
                            listMarcas.add(Marcas(marcaClaro, tiempoAire, megas, megas))
                            listMarcas.add(Marcas(marcaTuenti, tiempoAire, tiempoAire, tiempoAire))
                            listMarcas.add(Marcas(marcaEntel, tiempoAire, tiempoAire, tiempoAire))
                            listMarcas.forEach {
                                val insert = suspend {
                                    dbUtils.startInsertContent(
                                        it,
                                        BaseApplication.getRoom()
                                    )
                                }
                                insert.invoke()
                            }
                            onFinishedListener.onFinishedLogin(response.body())
                        }
                    } else {
                        onFinishedListener.onFailureMessage(response.body()?.message)
                    }
                } else {
                    onFinishedListener.onFailureMessage(response.body()?.message)
                }
            }

            override fun onFailure(call: Call<ResponseLogin>, t: Throwable) {
                onFinishedListener.onFailure(t)
            }
        })
    }
}