package com.example.testnat.modules.splash

import android.os.Bundle
import com.example.testnat.R
import com.example.testnat.modules.base.BaseActivity
import android.content.Intent
import android.os.Handler
import com.example.testnat.modules.home.HomeActivity
import com.example.testnat.modules.login.LoginActivity
import com.example.testnat.utilities.Constants
import com.example.testnat.utilities.Constants.Companion.TOKEN_ACCESS_PREFERENCE_KEY
import com.example.testnat.utilities.SharedPreferencesUtils.Companion.getDataString

private const val SPLASH_TIME_OUT = 5000

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.SplashTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed(Runnable {
            val i = if (getDataString(TOKEN_ACCESS_PREFERENCE_KEY) != null && getDataString(
                    TOKEN_ACCESS_PREFERENCE_KEY
                ) != ""
            ) {
                Intent(this@SplashActivity, HomeActivity::class.java)
            } else {
                Intent(this@SplashActivity, LoginActivity::class.java)
            }
            startActivity(i)
            finish()
        }, SPLASH_TIME_OUT.toLong())
    }
}