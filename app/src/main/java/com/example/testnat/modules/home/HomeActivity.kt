package com.example.testnat.modules.home

import android.os.Bundle
import com.example.testnat.R
import com.example.testnat.modules.base.BaseActivity

class HomeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }

}