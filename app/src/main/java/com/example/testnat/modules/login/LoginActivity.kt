package com.example.testnat.modules.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.example.testnat.R
import com.example.testnat.models.Marcas
import com.example.testnat.models.ResponseLogin
import com.example.testnat.models.TokenManager.Companion.setTokenAuth
import com.example.testnat.modules.base.BaseActivity
import com.example.testnat.modules.base.BaseApplication
import com.example.testnat.modules.home.HomeActivity
import com.example.testnat.utilities.Constants.Companion.BASIC_AUTH
import com.example.testnat.utilities.Constants.Companion.EXPIRES_IN_PREFERENCE_KEY
import com.example.testnat.utilities.Constants.Companion.JTI_PREFERENCE_KEY
import com.example.testnat.utilities.Constants.Companion.PASSWORD_PREFERENCE_KEY
import com.example.testnat.utilities.Constants.Companion.SCOPE_PREFERENCE_KEY
import com.example.testnat.utilities.Constants.Companion.TOKEN_ACCESS_PREFERENCE_KEY
import com.example.testnat.utilities.Constants.Companion.TOKEN_REFRESH_PREFERENCE_KEY
import com.example.testnat.utilities.Constants.Companion.TOKEN_TYPE_PREFERENCE_KEY
import com.example.testnat.utilities.Constants.Companion.USERNAME_PREFERENCE_KEY
import com.example.testnat.utilities.EncryptionTool.convertirSHA256
import com.example.testnat.utilities.SharedPreferencesUtils
import com.example.testnat.utilities.storage.DatabaseUtils
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class LoginActivity : BaseActivity(), LoginContract.View, CoroutineScope {

    private val presenter: LoginContract.Presenter = LoginPresenter(this)
    private var user: String? = null
    private var password: String? = null
    private var listMarcas: MutableList<Marcas> = mutableListOf()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job
    private lateinit var job: Job



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        job = Job()
        initViews()
    }

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }

    override fun initViews() {
        setTokenAuth(BASIC_AUTH)
        button_login.setOnClickListener {
            if (validateForm()) {
                user?.let { it1 -> password?.let { it2 -> login(it1, it2) } }
            }
        }
    }

    private fun insert() {
        /*launch {
            listMarcas.forEach {
                val insert = suspend {
                    dbUtils.startInsertContent(
                        it,
                        BaseApplication.getRoom()
                    )
                }
                insert.invoke()
                /*val job2 = GlobalScope.launch(Dispatchers.Main) {
                    insert.invoke()
                }*/
            }
        }*/
    }

    private fun validateForm(): Boolean {
        if (!validateInputSize(inputUser, getString(R.string.error_user_null), 0)) {
            return false
        }
        if (!validateInputSize(inputPass, getString(R.string.error_password_null), 0)) {
            return false
        }
        user = inputUser.text.toString().trim()
        password = convertirSHA256(inputPass.text.toString().trim())
        return true
    }

    private fun validateInputSize(
        editText: TextInputEditText,
        messageError: String?,
        size: Int,
    ): Boolean {
        return when {
            (editText.text == null) || (editText.length() <= size) -> {
                editText.error = messageError
                false
            }
            else -> {
                editText.error = null
                true
            }
        }
    }

    private fun login(user: String, password: String) {
        presenter.login(user, password)
    }

    override fun onFinishedLogin(responseLogin: ResponseLogin?) {
        inputUser.setText("")
        inputPass.setText("")
        if (responseLogin?.refresh_token != null) {
            user?.let { SharedPreferencesUtils.setDataString(USERNAME_PREFERENCE_KEY, it) }
            password?.let { SharedPreferencesUtils.setDataString(PASSWORD_PREFERENCE_KEY, it) }
            responseLogin.accessToken?.let {
                SharedPreferencesUtils.setDataString(
                    TOKEN_ACCESS_PREFERENCE_KEY,
                    it
                )
            }
            responseLogin.refresh_token?.let {
                SharedPreferencesUtils.setDataString(
                    TOKEN_REFRESH_PREFERENCE_KEY,
                    it
                )
            }
            responseLogin.expires_in?.let {
                SharedPreferencesUtils.setDataString(
                    EXPIRES_IN_PREFERENCE_KEY,
                    it
                )
            }
            responseLogin.tokenType?.let {
                SharedPreferencesUtils.setDataString(
                    TOKEN_TYPE_PREFERENCE_KEY,
                    it
                )
            }
            responseLogin.scope?.let {
                SharedPreferencesUtils.setDataString(
                    SCOPE_PREFERENCE_KEY,
                    it
                )
            }
            responseLogin.jti?.let { SharedPreferencesUtils.setDataString(JTI_PREFERENCE_KEY, it) }
            nextFragment()
        }
    }

    override fun onResponseFailure(throwable: Throwable?) {
        val message = throwable?.localizedMessage
        val messageToShow =
            if (message != null && message != "null" && message != "" && message != " ") message else getString(
                R.string.error_login
            )
        Toast.makeText(this, messageToShow, Toast.LENGTH_SHORT).show()
    }

    override fun onResponseMessage(message: String?) {
        val messageToShow =
            if (message != null && message != "null" && message != "" && message != " ") message else getString(
                R.string.error_login
            )
        Toast.makeText(this, messageToShow, Toast.LENGTH_SHORT).show()
    }

    private fun nextFragment() {
        inputUser.text = null
        inputPass.text = null
        val intent = Intent(
            this,
            HomeActivity::class.java
        )
        startActivity(intent)
    }
}