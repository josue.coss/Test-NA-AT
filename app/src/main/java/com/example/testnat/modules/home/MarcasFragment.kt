package com.example.testnat.modules.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testnat.R
import com.example.testnat.adapters.AdapterMarcas
import com.example.testnat.adapters.AdapterOptions
import com.example.testnat.listeners.ClickListener
import com.example.testnat.listeners.MarcasListener
import com.example.testnat.models.InfoMarcas
import com.example.testnat.models.Marcas
import com.example.testnat.modules.base.BaseApplication
import com.example.testnat.utilities.Constants
import com.example.testnat.utilities.Constants.Companion.DATABASE_MARCAS
import com.example.testnat.utilities.storage.DatabaseUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MarcasFragment : Fragment(R.layout.fragment_marcas) {

    private var recyclerViewMarcas: RecyclerView? = null
    private var layoutManagerMarcas: RecyclerView.LayoutManager? = null
    private var adapterMarcas: AdapterMarcas? = null
    private var listMarcas: MutableList<Marcas> = mutableListOf()
    private var recyclerViewOptions: RecyclerView? = null
    private var layoutManagerOptions: RecyclerView.LayoutManager? = null
    private var adapterOptions: AdapterOptions? = null
    private var listOptions: MutableList<String> = mutableListOf()
    var marcasContent: List<InfoMarcas>? = null
    val dbUtils = object : DatabaseUtils.MarcasDBListener {
        override fun onFinishedinsertContent(flag: Boolean) {

        }

        override fun onFinishedGetContent(content: Any?, finished: String?) {
            if (content != null && finished == DATABASE_MARCAS) {
                Log.d("onFinishedGetContent", content.toString())
                marcasContent = content as List<InfoMarcas>?
                marcasContent?.forEach {
                    val marca = Marcas(it.marca, it.subMarca1, it.subMarca2, it.subMarca3)
                    listMarcas.add(marca)
                }
                adapterMarcas?.notifyDataSetChanged()
            }
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_marcas, container, false)
        recyclerViewMarcas = view.findViewById(R.id.rvMarcas) as RecyclerView
        recyclerViewOptions = view.findViewById(R.id.rvOptions) as RecyclerView
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initViews() {
        getMarcas()
        val recargas = getString(R.string.recargas)
        val recaudacion = getString(R.string.recaudacion)
        val administracion = getString(R.string.administracion)
        listOptions.add(recargas)
        listOptions.add(recaudacion)
        listOptions.add(administracion)
        adapterMarcas = AdapterMarcas(
            activity as AppCompatActivity,
            listMarcas,
            object : MarcasListener {
                override fun onVerMas(posicion: Int) {
                    nextFragment()
                }
            }
        )
        recyclerViewMarcas?.adapter = adapterMarcas
        layoutManagerMarcas = LinearLayoutManager(activity as AppCompatActivity)
        recyclerViewMarcas?.layoutManager = layoutManagerMarcas
        adapterMarcas?.notifyDataSetChanged()

        adapterOptions = AdapterOptions(
            activity as AppCompatActivity,
            listOptions,
            object : ClickListener {
                override fun onClick(vista: View, posicion: Int) {

                }
            }
        )
        recyclerViewOptions?.adapter = adapterOptions
        layoutManagerOptions = LinearLayoutManager(
            activity as AppCompatActivity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        recyclerViewOptions?.layoutManager = layoutManagerOptions
        adapterOptions?.notifyDataSetChanged()
    }

    private fun getMarcas() {
        lifecycleScope.launch {
            val get = suspend {
                dbUtils.startGetMarcasContent(BaseApplication.getRoom())
            }
            get.invoke()
            /*val job = GlobalScope.launch(Dispatchers.Main) {
                get.invoke()
            }*/
        }
    }

    private fun nextFragment() {
        view?.findNavController()?.navigate(R.id.action_marcasFragment_to_recargasFragment)
    }
}