package com.example.testnat.modules.base

import android.app.Application
import android.content.Context
import com.example.testnat.utilities.storage.AppDatabase

class BaseApplication : Application() {
    companion object {
        var instance: BaseApplication? = null

        fun getRoom(): AppDatabase? {
            return getContext()?.let { AppDatabase.getInstance(it) }
        }

        fun getContext(): Context? {
            return instance?.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}