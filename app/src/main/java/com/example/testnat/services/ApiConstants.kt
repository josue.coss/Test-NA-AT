package com.example.testnat.services

class ApiConstants {
    companion object {
        const val BASE_AUTH_URL = "https://uat.firmaautografa.com/authorization-server/"
        const val BASE_URL = "https://uat.firmaautografa.com/"
        const val LOGIN = "oauth/token"
    }
}