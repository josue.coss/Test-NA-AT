package com.example.testnat.services

import com.example.testnat.models.TokenManager
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class AuthInterceptor : Interceptor {

    private val TOKEN_TYPE = "Basic"

    @Synchronized
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()

        val builder = original.newBuilder()
            .header(
                TokenAuthenticator.AUTH_TAG,
                "$TOKEN_TYPE ${TokenManager.getTokenAuth()}"
            )

        val request = builder.build()
        return chain.proceed(request)
    }
}