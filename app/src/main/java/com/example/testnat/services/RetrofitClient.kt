package com.example.testnat.services

import com.example.testnat.services.ApiConstants.Companion.BASE_AUTH_URL
import com.example.testnat.services.ApiConstants.Companion.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient(type: ClientType) {

    val api: ApplicationApi


    init {
        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)

        if (type == ClientType.BASE_WITH_AUTH) {
            okHttpClient.addInterceptor(AuthInterceptor())
        }


        okHttpClient.addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        })

        val retrofit = Retrofit.Builder()
            .baseUrl(getBaseUrl(type))
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient.build())
            .build()

        api = retrofit.create(ApplicationApi::class.java)
    }

    private fun getBaseUrl(clientType: ClientType): String {
        return when (clientType) {
            ClientType.BASE -> {
                BASE_URL
            }
            ClientType.BASE_WITH_AUTH -> {
                BASE_AUTH_URL
            }
        }
    }

    companion object {

        fun getInstance(type: ClientType): RetrofitClient {
            return RetrofitClient(type)
        }

    }

    enum class ClientType {
        BASE, BASE_WITH_AUTH
    }

}
