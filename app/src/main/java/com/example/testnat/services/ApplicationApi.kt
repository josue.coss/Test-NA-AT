package com.example.testnat.services

import com.example.testnat.models.ResponseLogin
import com.example.testnat.utilities.Constants.Companion.GRANT_TYPE
import com.example.testnat.utilities.Constants.Companion.PASSWORD
import com.example.testnat.utilities.Constants.Companion.USER_NAME
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST


interface ApplicationApi {

    @FormUrlEncoded
    @POST(ApiConstants.LOGIN)
    @Headers("Content-Type: application/x-www-form-urlencoded")
    fun login(
        @Field(GRANT_TYPE) grantType: String? = PASSWORD,
        @Field(USER_NAME) username: String?,
        @Field(PASSWORD) password: String?,
    ): Call<ResponseLogin>
}