package com.example.testnat.components

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.ViewGroup
import android.view.Window
import com.example.testnat.R
import com.example.testnat.listeners.OnOkListener
import com.example.testnat.utilities.Constants.Companion.EXITO_COMPONENT_TAG
import kotlinx.android.synthetic.main.dialog_exito_component.*

class ExitoComponent(
    private var c: Context,
    onOKListener: OnOkListener,
) : Dialog(c) {

    private var onOkListener: OnOkListener? = onOKListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        setContentView(R.layout.dialog_exito_component)
        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setCancelable(false)
        button_ok.setOnClickListener {
            onOkListener?.onOk(EXITO_COMPONENT_TAG)
            dismiss()
        }
    }
}
