package com.example.testnat.components

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.ViewGroup
import android.view.Window
import com.example.testnat.R
import com.example.testnat.listeners.OnOkListener
import com.example.testnat.utilities.Constants.Companion.RECARGA_COMPONENT_TAG
import kotlinx.android.synthetic.main.dialog_recarga_component.*


class RecargaComponent(
    private var c: Context,
    onOKListener: OnOkListener,
    user: String,
    hour: String,
    date: String,
    phone: String,
    cash: String,
) : Dialog(c) {

    private var onOkListener: OnOkListener? = onOKListener
    private var user: String = user
    private var hour: String = hour
    private var date: String = date
    private var phone: String = phone
    private var cash: String = cash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        setContentView(R.layout.dialog_recarga_component)
        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        setCancelable(false)
        tvUser.text = user
        tvHour.text = hour
        tvDate.text = date
        tvPhone.text = phone
        tvCash.text = cash
        button_cancel.setOnClickListener {
            dismiss()
        }
        button_next.setOnClickListener {
            onOkListener?.onOk(RECARGA_COMPONENT_TAG)
            dismiss()
        }
    }
}
