package com.example.testnat.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.testnat.R
import com.example.testnat.listeners.ClickListener
import kotlinx.android.synthetic.main.list_item_options.view.*

class AdapterOptions(
    val contexT: Context,
    val listOptions: MutableList<String>,
    var clickListener: ClickListener,
) : RecyclerView.Adapter<AdapterOptions.OptionsViewHolder?>() {
    var context = contexT
    var options = listOptions
    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    class OptionsViewHolder(itemView: View, listener: ClickListener) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var tvOption: TextView = itemView.tvOption
        var listener: ClickListener? = null
        override fun onClick(v: View?) {
            this.listener?.onClick(v!!, adapterPosition)
        }

        init {
            this.listener = listener
            itemView.setOnClickListener(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionsViewHolder {
        val itemView: View = layoutInflater.inflate(R.layout.list_item_options, parent, false)
        return OptionsViewHolder(itemView, clickListener)
    }

    override fun onBindViewHolder(holder: OptionsViewHolder, position: Int) {
        val option: String = options.get(position)
        holder.tvOption.text = option
    }

    override fun getItemCount(): Int {
        return options.size
    }
}
