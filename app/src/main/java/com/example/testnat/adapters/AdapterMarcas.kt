package com.example.testnat.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.testnat.adapters.AdapterMarcas.MarcasViewHolder
import com.example.testnat.R
import com.example.testnat.listeners.MarcasListener
import com.example.testnat.models.Marcas
import kotlinx.android.synthetic.main.list_item_marcas.view.*

class AdapterMarcas(
    val contexT: Context,
    val marcaS: MutableList<Marcas>,
    var clickListener: MarcasListener,
) : RecyclerView.Adapter<MarcasViewHolder?>() {
    var context = contexT
    var marcas = marcaS
    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    class MarcasViewHolder(itemView: View, listener: MarcasListener) :
        RecyclerView.ViewHolder(itemView) {
        var tvMarca: TextView = itemView.tvMarca
        var tvVerMas: TextView = itemView.tvVerMas
        var tvMarca1: TextView = itemView.tvSubMarca1
        var tvMarca2: TextView = itemView.tvSubMarca2
        var tvMarca3: TextView = itemView.tvSubMarca3
        var iconLogoSub1: ImageView = itemView.iconoLogoSubMarca1
        var iconLogoSub2: ImageView = itemView.iconoLogoSubMarca2
        var iconLogoSub3: ImageView = itemView.iconoLogoSubMarca3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarcasViewHolder {
        val itemView: View = layoutInflater.inflate(R.layout.list_item_marcas, parent, false)
        return MarcasViewHolder(itemView, clickListener)
    }

    override fun onBindViewHolder(holder: MarcasViewHolder, position: Int) {
        val marca: Marcas = marcas.get(position)
        customizeView(holder, marca, position)
    }

    fun customizeView(holder: MarcasViewHolder, marca: Marcas, position: Int) {
        holder.tvMarca.text = marca.marca
        holder.tvMarca1.text = marca.subMarca1
        holder.tvMarca2.text = marca.subMarca2
        holder.tvMarca3.text = marca.subMarca3
        when (marca.marca) {
            context.getString(R.string.marca_claro) -> {
                holder.iconLogoSub1.setImageResource(R.mipmap.ic_claro)
                holder.iconLogoSub2.setImageResource(R.mipmap.ic_claro)
                holder.iconLogoSub3.setImageResource(R.mipmap.ic_claro)
            }
            context.getString(R.string.marca_tuenti) -> {
                holder.iconLogoSub1.setImageResource(R.mipmap.ic_tuenti)
                holder.iconLogoSub2.setImageResource(R.mipmap.ic_tuenti)
                holder.iconLogoSub3.setImageResource(R.mipmap.ic_tuenti)
            }
            context.getString(R.string.marca_entel) -> {
                holder.iconLogoSub1.setImageResource(R.mipmap.ic_entel)
                holder.iconLogoSub2.setImageResource(R.mipmap.ic_entel)
                holder.iconLogoSub3.setImageResource(R.mipmap.ic_entel)
            }
            else -> {
                holder.iconLogoSub1.setImageResource(R.mipmap.ic_logo)
                holder.iconLogoSub2.setImageResource(R.mipmap.ic_logo)
                holder.iconLogoSub3.setImageResource(R.mipmap.ic_logo)
            }
        }
        holder.tvVerMas.setOnClickListener {
            this.clickListener.onVerMas(position)
        }
    }

    override fun getItemCount(): Int {
        return marcas.size
    }
}
