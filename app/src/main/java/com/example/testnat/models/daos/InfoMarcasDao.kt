package com.example.testnat.models.daos

import androidx.room.*
import com.example.testnat.models.InfoMarcas

@Dao
interface InfoMarcasDao {

    @Query("SELECT * FROM infoMarcas")
    fun getInfoMarcas(): List<InfoMarcas>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMarca(vararg marca: InfoMarcas)

    @Query("DELETE FROM infoMarcas")
    fun deleteMarca()

    @Update
    fun updateInfoMarca(vararg infoMarca: InfoMarcas)
}