package com.example.testnat.models

import com.example.testnat.utilities.SharedPreferencesUtils

class TokenManager {

    companion object {

        private val TOKEN_TAG = "token"

        fun setTokenAuth(token: String?) {
            token?.let { SharedPreferencesUtils.setDataString(TOKEN_TAG, it) }
        }

        fun getTokenAuth(): String? {
            val token = SharedPreferencesUtils.getDataString(TOKEN_TAG)
            return if (token == null || token == "") {
                "-"
            } else {
                token
            }
        }
    }
}