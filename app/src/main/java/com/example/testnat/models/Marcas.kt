package com.example.testnat.models

data class Marcas(
    var marca: String? = "",
    var subMarca1: String? = "",
    var subMarca2: String? = "",
    var subMarca3: String? = "",
)
