package com.example.testnat.models

import com.google.gson.annotations.SerializedName

data class ResponseLogin(
    @SerializedName("timestamp") val timestamp: String?,
    @SerializedName("status") val status: Int?,
    @SerializedName("error") val error: String?,
    @SerializedName("message") val message: String?,
    @SerializedName("path") val path: String?,
    @SerializedName("access_token") val accessToken: String?,
    @SerializedName("token_type") val tokenType: String?,
    @SerializedName("refresh_token") val refresh_token: String?,
    @SerializedName("expires_in") val expires_in: String?,
    @SerializedName("scope") val scope: String?,
    @SerializedName("jti") val jti: String?,
    @SerializedName("success") val success: String?,
)
