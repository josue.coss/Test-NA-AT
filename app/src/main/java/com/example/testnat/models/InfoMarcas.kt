package com.example.testnat.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "infoMarcas")
data class InfoMarcas(
    @PrimaryKey var marca: String,
    @ColumnInfo(name = "subMarca1") var subMarca1: String?,
    @ColumnInfo(name = "subMarca2") var subMarca2: String?,
    @ColumnInfo(name = "subMarca3") var subMarca3: String?,
)
